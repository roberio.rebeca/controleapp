# Sistema de Controle Escolar
`Versão 1.0.0`

Objetivo deste sistema é o controle escolar dos filhos
* * *
## Estrutura
* Core
    - Status
* Cadastros
    - Cidade/UF
    - Escola
    - Série
    - Professor
    - Materia
    - Aluno 
* Lançamentos
    - Matricula
    - Tarefas
    - Trabalhos
    - Provas
        - Mensal
        - Bimestral
        - Trimestral
    - Boletim
* Consultas
    - Tarefas Realizadas
    - Tarefas Não Realizadas
    - Trabalhos Realizados
    - Trabalhos Não Realizados
    - Provas
* * *
## Andamento do Desenvolvimento do Sistema
- [x] Analise Situação Problema
- [x] Elaborar a Estrutura do Sistema
- Estruturar o Backend
    - Configurações
        - [x] Status
    - Cadastros
        - [x] Cidade/UF
        - [x] Escola
        - [x] Série
        - [x] Professor
        - [x] Matéria
        - [x] Aluno
    - Lançamentos
        - [x] Matricula
        - [x] Tarefas
        - [x] Trabalhos
        - Provas
            - [x] Mensal
            - [x] Bimetral
            - [x] Trimestral
        - [x] Boletim
    - Consultas
        - [ ] Tarefas Realizadas
        - [ ] Tarefas Não Realizadas
        - [ ] Trabalhos Realizados
        - [ ] Trabalhos Não Realizados
        - [ ] Provas
* * *
## Saiba mais...
Para mais informações técnica e estrutural, acesse os links abaixo

[Técnico]:/docs/README-Tecnico.md
[Estrutural]:/docs/README-Estrutural.md

- Dados [Técnico][].
- Dados [Estrutural][].
