from django.db import models
from django.utils.translation import gettext as _

from controleapp.core.models import Status
from controleapp.register.models import Course


class Inscription(models.Model):
    """
    Model to store Inscription data
    """
    school = models.ForeignKey(verbose_name=_('school'),
                               to='register.School',
                               on_delete=models.DO_NOTHING
                               )
    room = models.ForeignKey(verbose_name=_('room'),
                             to='register.Room',
                             on_delete=models.DO_NOTHING
                             )
    student = models.ForeignKey(verbose_name=_('student'),
                                to='register.Student',
                                on_delete=models.DO_NOTHING
                                )
    startDate = models.DateField(verbose_name=_('startDate')
                                 )
    stopDate = models.DateField(verbose_name=_('stopData'),
                                null=True,
                                blank=True
                                )
    status = models.ForeignKey(verbose_name=_('status'),
                               to='core.Status',
                               on_delete=models.DO_NOTHING
                               )
    dateRegister = models.DateTimeField(verbose_name=_('dateRegister'),
                                        auto_now_add=True
                                        )

    class Meta:
        verbose_name = _('inscription')
        verbose_name_plural = _('inscriptions')

    def __str__(self):
        # return self.student.name
        return f'{self.room.description} - {self.student.name}'

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        courses = Course.objects.all()

        for c in courses:
            InscriptionDetail.objects.update_or_create(inscription=self, course=c)


class InscriptionDetail(models.Model):
    inscription = models.ForeignKey(verbose_name=_('inscription'),
                                    to='entry.Inscription',
                                    on_delete=models.CASCADE
                                    )
    course = models.CharField(verbose_name=_('course'),
                              max_length=50
                              )
    teacher = models.ForeignKey(verbose_name=_('teacher'),
                                to='register.Teacher',
                                on_delete=models.DO_NOTHING,
                                null=True,
                                blank=True
                                )

    def __str__(self):
        return f'{self.course} - {self.teacher}'


class Task(models.Model):
    """
    Model to store Task data
    """
    description = models.CharField(verbose_name=_('description'),
                                   max_length=80
                                   )
    inscription = models.ForeignKey(verbose_name=_('inscription'),
                                    to='entry.Inscription',
                                    on_delete=models.DO_NOTHING
                                    )
    courseTeacher = models.ForeignKey(verbose_name=_('courseTeacher'),
                                      to='entry.InscriptionDetail',
                                      on_delete=models.DO_NOTHING
                                      )

    class Meta:
        verbose_name = _('task')
        verbose_name_plural = _('tasks')

    def __str__(self):
        return self.description


class TaskDetail(models.Model):
    task = models.ForeignKey(verbose_name=_('task'),
                             to='entry.Task',
                             on_delete=models.CASCADE
                             )
    dateTask = models.DateField(verbose_name=_('dateTask')
                                )
    status = models.IntegerField(default=Status.UNCHECK,
                                 choices=Status.STATUS_TASK
                                 )

    def __str__(self):
        return self.task.description


class Job(models.Model):
    """
    Model to store Job Data
    """
    description = models.CharField(verbose_name=_('description'),
                                   max_length=80
                                   )
    inscription = models.ForeignKey(verbose_name=_('inscription'),
                                    to='entry.Inscription',
                                    on_delete=models.DO_NOTHING
                                    )
    courseTeacher = models.ForeignKey(verbose_name=_('courseTeacher'),
                                      to='entry.InscriptionDetail',
                                      on_delete=models.DO_NOTHING
                                      )

    class Meta:
        verbose_name = _('job')
        verbose_name_plural = _('works')

    def __str__(self):
        return self.description


class JobDetail(models.Model):
    job = models.ForeignKey(verbose_name=_('job'),
                            to='entry.Job',
                            on_delete=models.CASCADE
                            )
    dateJob = models.DateField(verbose_name=_('dateJob')
                               )
    status = models.IntegerField(default=Status.UNCHECK,
                                 choices=Status.STATUS_TASK
                                 )

    def __str__(self):
        return self.job.description


class TestMonthly(models.Model):
    """
    Model to store TestMonthly data
    """
    description = models.CharField(verbose_name=_('description'),
                                   max_length=80
                                   )
    inscription = models.ForeignKey(verbose_name=_('inscription'),
                                    to='entry.Inscription',
                                    on_delete=models.DO_NOTHING
                                    )
    courseTeacher = models.ForeignKey(verbose_name=_('courseTeacher'),
                                      to='entry.InscriptionDetail',
                                      on_delete=models.DO_NOTHING
                                      )

    class Meta:
        verbose_name = _('testMonthly')
        verbose_name_plural = _('testMonthly')

    def __str__(self):
        return self.description


class TestMonthlyDetail(models.Model):
    test = models.ForeignKey(verbose_name=_('test'),
                             to='entry.TestMonthly',
                             on_delete=models.CASCADE
                             )
    dateTestMonthly = models.DateField(verbose_name=_('dateTestMonthly')
                                       )
    rating = models.DecimalField(verbose_name=_('rating'),
                                 max_digits=5,
                                 decimal_places=2,
                                 default=0
                                 )
    note = models.DecimalField(verbose_name=_('note'),
                               max_digits=5,
                               decimal_places=2,
                               default=0
                               )

    def __str__(self):
        return self.test.description


class TestBimonthly(models.Model):
    """
    Model to store TestBimonthly data
    """
    description = models.CharField(verbose_name=_('description'),
                                   max_length=80
                                   )
    inscription = models.ForeignKey(verbose_name=_('inscription'),
                                    to='entry.Inscription',
                                    on_delete=models.DO_NOTHING
                                    )
    courseTeacher = models.ForeignKey(verbose_name=_('courseTeacher'),
                                      to='entry.InscriptionDetail',
                                      on_delete=models.DO_NOTHING
                                      )

    class Meta:
        verbose_name = _('testBimonthly')
        verbose_name_plural = _('testBimonthly')

    def __str__(self):
        return self.description


class TestBimonthlyDetail(models.Model):
    test = models.ForeignKey(verbose_name=_('test'),
                             to='entry.TestBimonthly',
                             on_delete=models.CASCADE
                             )
    dateTestBimonthly = models.DateField(verbose_name=_('dateTestBimonthly')
                                         )
    rating = models.DecimalField(verbose_name=_('rating'),
                                 max_digits=5,
                                 decimal_places=2,
                                 default=0
                                 )
    note = models.DecimalField(verbose_name=_('note'),
                               max_digits=5,
                               decimal_places=2
                               )

    def __str__(self):
        return self.test.description


class Bulletin(models.Model):
    """
    Model to store Bulletin data
    """
    inscription = models.ForeignKey(verbose_name=_('inscription'),
                                    to='entry.Inscription',
                                    on_delete=models.DO_NOTHING
                                    )

    class Meta:
        verbose_name = _('bulletin')
        verbose_name_plural = _('bulletins')

    def __str__(self):
        return f'{self.inscription.room.description} - {self.inscription.student.name}'

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        courses = Course.objects.all()

        for c in courses:
            BulletinDetail.objects.update_or_create(bulletin=self, course=c)


class BulletinDetail(models.Model):
    bulletin = models.ForeignKey(verbose_name=_('bulletin'),
                                 to='entry.Bulletin',
                                 on_delete=models.CASCADE
                                 )
    course = models.CharField(verbose_name=_('course'),
                              max_length=80
                              )
    quarterOne = models.DecimalField(verbose_name=_('quarterOne'),
                                     max_digits=5,
                                     decimal_places=2,
                                     default=0
                                     )
    quarterTwo = models.DecimalField(verbose_name=_('quarterTwo'),
                                     max_digits=5,
                                     decimal_places=2,
                                     default=0
                                     )
    quarterThree = models.DecimalField(verbose_name=_('quarterThree'),
                                       max_digits=5,
                                       decimal_places=2,
                                       default=0
                                       )
    quarterFor = models.DecimalField(verbose_name=_('quarterFor'),
                                     max_digits=5,
                                     decimal_places=2,
                                     default=0
                                     )

    def __str__(self):
        return str(self.course)


class Horary(models.Model):
    """
    Model to store Horary data
    """
    inscription = models.ForeignKey(verbose_name=_('inscription'),
                                    to='entry.Inscription',
                                    on_delete=models.DO_NOTHING
                                    )
    status = models.IntegerField(default=Status.ACTIVE,
                                 choices=Status.STATUS_DISPLAY
                                 )
    dateRegister = models.DateTimeField(verbose_name=_('dateRegister'))

    class Meta:
        verbose_name = _('horary')
        verbose_name = _('horaries')

    def __str__(self):
        return self.inscription.student.name

    def save(self, *args, **kwargs):
        weekdays = (_('monday'),
                    _('tuesday'),
                    _('wednesday'),
                    _('thursday'),
                    _('friday'))
        super().save(*args, **kwargs)
        for d in weekdays:
            HoraryDetail.objects.update_or_create(horary=self, weekday=d)


class HoraryDetail(models.Model):
    horary = models.ForeignKey(verbose_name=_('horary'),
                               to='entry.Horary',
                               on_delete=models.CASCADE
                               )
    weekday = models.CharField(verbose_name=_('weekday'),
                               max_length=40)
    lessonOne = models.ForeignKey(verbose_name=_('lessonOne'),
                                  to='register.Course',
                                  on_delete=models.DO_NOTHING,
                                  related_name='lessonOneCourse',
                                  null=True,
                                  blank=True
                                  )
    lessonTwo = models.ForeignKey(verbose_name=_('lessonTwo'),
                                  to='register.Course',
                                  on_delete=models.DO_NOTHING,
                                  related_name='lessonTwoCourse',
                                  null=True,
                                  blank=True
                                  )
    lessonThree = models.ForeignKey(verbose_name=_('lessonThree'),
                                    to='register.Course',
                                    on_delete=models.DO_NOTHING,
                                    related_name='lessonThreeCourse',
                                    null=True,
                                    blank=True
                                    )
    lessonFor = models.ForeignKey(verbose_name=_('lessonFor'),
                                  to='register.Course',
                                  on_delete=models.DO_NOTHING,
                                  related_name='lessonForCourse',
                                  null=True,
                                  blank=True
                                  )

    def __str__(self):
        return str(self.horary)
