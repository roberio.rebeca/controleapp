from django.contrib import admin

from .models import Inscription, InscriptionDetail, Task, TaskDetail, Job, JobDetail, TestMonthly, TestMonthlyDetail, \
    TestBimonthly, TestBimonthlyDetail, Bulletin, BulletinDetail, Horary, HoraryDetail


class InscriptionDetailInline(admin.TabularInline):
    model = InscriptionDetail


@admin.register(Inscription)
class InscriptionAdmin(admin.ModelAdmin):
    autocomplete_fields = ('school', 'room', 'student')
    list_display = ('student', 'room')
    list_filter = ('school__nickname',)
    search_fields = ('student__name',)
    inlines = [
        InscriptionDetailInline
    ]


class TaskDetailInline(admin.StackedInline):
    model = TaskDetail


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('description', 'courseTeacher')
    list_filter = ('inscription',)
    ordering = ('description',)
    inlines = [
        TaskDetailInline
    ]


class JobDetailInline(admin.StackedInline):
    model = JobDetail


@admin.register(Job)
class JobAdmin(admin.ModelAdmin):
    list_display = ('description', 'courseTeacher')
    list_filter = ('inscription',)
    ordering = ('description',)
    inlines = [
        JobDetailInline
    ]


class TestMonthlyDetailInline(admin.StackedInline):
    model = TestMonthlyDetail


@admin.register(TestMonthly)
class TestMonthlyAdmin(admin.ModelAdmin):
    list_display = ('description', 'courseTeacher')
    list_filter = ('inscription',)
    ordering = ('description',)
    inlines = [
        TestMonthlyDetailInline
    ]


class TestBimonthlyDetailInline(admin.StackedInline):
    model = TestBimonthlyDetail


@admin.register(TestBimonthly)
class TestBimonthlyAdmin(admin.ModelAdmin):
    list_display = ('description', 'courseTeacher')
    list_filter = ('inscription',)
    ordering = ('description',)
    inlines = [
        TestBimonthlyDetailInline
    ]


class BulletinDetailInline(admin.TabularInline):
    model = BulletinDetail


@admin.register(Bulletin)
class BulletinAdmin(admin.ModelAdmin):
    list_display = ('inscription',)
    ordering = ('inscription',)
    inlines = [
        BulletinDetailInline
    ]


class HoraryDetailInline(admin.TabularInline):
    model = HoraryDetail


@admin.register(Horary)
class HoraryAdmin(admin.ModelAdmin):
    inlines = [
        HoraryDetailInline
    ]
