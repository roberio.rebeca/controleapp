from django.db import models
from django.utils.translation import gettext as _

from controleapp.core.models import Status


class City(models.Model):
    """
    Model to store City and State data
    """
    name = models.CharField(verbose_name=_('name'),
                            max_length=50
                            )
    uf = models.CharField(verbose_name=_('uf'),
                          max_length=2
                          )
    dateRegister = models.DateTimeField(verbose_name=_('dateRegister'),
                                        auto_now=True
                                        )

    class Meta:
        verbose_name = _('city')
        verbose_name_plural = _('cities')

    def __str__(self):
        return '%s %s' % (self.name, self.uf)


class School(models.Model):
    """
    Model to store School data
    """
    description = models.CharField(verbose_name=_('description'),
                                   max_length=100
                                   )
    nickname = models.CharField(verbose_name=_('nickname'),
                                max_length=50
                                )
    city = models.ForeignKey(verbose_name=_('city'),
                             to='register.City',
                             on_delete=models.DO_NOTHING
                             )
    status = models.IntegerField(verbose_name=_('status'),
                                 default=Status.ACTIVE,
                                 choices=Status.STATUS_DISPLAY
                                 )
    dateRegister = models.DateTimeField(verbose_name=_('dateRegister'),
                                        auto_now=True
                                        )

    class Meta:
        verbose_name = _('school')
        verbose_name_plural = _('schools')

    def __str__(self):
        return self.description


class Room(models.Model):
    """
    Model to store Room data
    """
    description = models.CharField(verbose_name=_('description'),
                                   max_length=10
                                   )
    school = models.ForeignKey(verbose_name=_('school'),
                               to='register.School',
                               on_delete=models.DO_NOTHING
                               )
    status = models.IntegerField(verbose_name=_('status'),
                                 default=Status.ACTIVE,
                                 choices=Status.STATUS_DISPLAY
                                 )
    dateRegister = models.DateTimeField(verbose_name=_('dateRegister'),
                                        auto_now=True
                                        )

    class Meta:
        verbose_name = _('room')
        verbose_name_plural = _('rooms')

    def __str__(self):
        return self.description


class Teacher(models.Model):
    """
    Model to store Teacher data
    """
    name = models.CharField(verbose_name=_('name'),
                            max_length=80
                            )
    city = models.ForeignKey(verbose_name=_('city'),
                             to='register.City',
                             on_delete=models.DO_NOTHING
                             )
    status = models.IntegerField(verbose_name=_('status'),
                                 default=Status.ACTIVE,
                                 choices=Status.STATUS_DISPLAY
                                 )
    dateRegister = models.DateTimeField(verbose_name=_('dateRegister'),
                                        auto_now=True
                                        )

    class Meta:
        verbose_name = _('teacher')
        verbose_name_plural = _('teachers')

    def __str__(self):
        return self.name


class Course(models.Model):
    """
    Model to store Course data
    """
    description = models.CharField(verbose_name=_('description'),
                                   max_length=80
                                   )
    status = models.IntegerField(verbose_name=_('status'),
                                 default=Status.ACTIVE,
                                 choices=Status.STATUS_DISPLAY
                                 )
    dateRegister = models.DateTimeField(verbose_name=_('dateRegister'),
                                        auto_now=True
                                        )

    class Meta:
        verbose_name = _('course')
        verbose_name_plural = _('courses')

    def __str__(self):
        return self.description


class Student(models.Model):
    """
    Model to store Student data
    """
    name = models.CharField(verbose_name=_('name'),
                            max_length=100
                            )
    birthDate = models.DateField(verbose_name=_('birthDate')
                                 )
    address = models.CharField(verbose_name=_('address'),
                               max_length=80
                               )
    city = models.ForeignKey(verbose_name=_('city'),
                             to='register.City',
                             on_delete=models.DO_NOTHING
                             )
    status = models.IntegerField(verbose_name=_('status'),
                                 default=Status.ACTIVE,
                                 choices=Status.STATUS_DISPLAY
                                 )
    dateRegister = models.DateTimeField(verbose_name=_('dateRegister'),
                                        auto_now=True
                                        )

    class Meta:
        verbose_name = _('student')
        verbose_name_plural = _('students')

    def __str__(self):
        return self.name
