from django.contrib import admin

from .models import City, School, Room, Teacher, Course, Student


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    ordering = ('name',)


@admin.register(School)
class SchoolAdmin(admin.ModelAdmin):
    autocomplete_fields = ('city',)
    list_filter = ('status',)
    search_fields = ('description',)
    ordering = ('description',)


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    list_display = ('description', 'school')
    autocomplete_fields = ('school',)
    list_filter = ('status',)
    search_fields = ('description',)
    ordering = ('school', 'description')


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    autocomplete_fields = ('city',)
    list_filter = ('status',)
    search_fields = ('name',)
    ordering = ('name',)


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    list_filter = ('status',)
    search_fields = ('description',)
    ordering = ('description',)


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    autocomplete_fields = ('city',)
    list_filter = ('status',)
    search_fields = ('name',)
    ordering = ('name',)
