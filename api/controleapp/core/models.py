from django.db import models
from django.utils.translation import gettext as _


class Status(models.Model):
    """
    Model to store Status data
    """
    ACTIVE = 1
    INACTIVE = 2
    STATUS_DISPLAY = (
        (ACTIVE, _('active')),
        (INACTIVE, _('inactive'))
    )
    CHECK = 1
    UNCHECK = 2
    STATUS_TASK = (
        (CHECK, _('CHECK')),
        (UNCHECK, _('UNCHECK'))
    )

    description = models.CharField(verbose_name=_('description'),
                                   max_length=50
                                   )
    dateRegister = models.DateTimeField(verbose_name=_('dateRegister'),
                                        auto_now=True
                                        )

    class Meta:
        verbose_name = _('status')
        verbose_name_plural = _('statuses')

    def __str__(self):
        return self.description
