# Sistema de Controle Escolar
`Versão 1.0.0`

Objetivo deste sistema é o controle escolar dos filhos
* * *
## Divisão do Sistema
### Backend
Inicialmente vamos trabalhar com o sistema, somente no Admin do Django
* * *
#### Core
* Status
    - Descrição
#### Cadastros
* Cidade/UF
    - Nome da Cidade
    - UF
* Escola
    - Descrição
    - Cidade/UF
    - Status
    - Data Cadastro
* Série
    - Descrição
    - Escola
    - Status
    - Data Cadastro
* Professor
    - Nome
    - Cidade
    - Status
    - Data Cadastro
* Materia
    - Descrição
    - Status
    - Data Cadatros
* Aluno 
    - Nome
    - Data Nascimento
    - Endereço
    - Cidade
    - Status
    - Data Cadastro
#### Lançamentos
* Matricula
    - Escola
    - Série
    - Aluno
    - Inicio das Aulas
    - Final das Aulas
    - Status
    - Data Cadastro
        * Matéria
        * Professor
* Tarefas
    - Matricula
    - Matéria/Professor
        - Data
        - Status
* Trabalhos
    - Matricula
    - Matéria/Professor
        - Data
        - Status
* Provas Mensal
    - Matricula
    - Matéria/Professor
        - Data
        - Nota
* Provas Bimestral
    - Matricula
    - Matéria/Professor
        - Data
        - Nota
* Provas Trimestral
    - Matricula
    - Matéria/Professor
        - Data
        - Nota
* Boletins
    - Matricula
        - Matéria
        - Nota Primeiro Trimestre
        - Nota Segundo Trimestre
        - Nota Terceiro Trimestre
        - Nota Quarto Trimestre
* Consultas
    - Tarefas Realizadas
    - Tarefas Não Realizadas
    - Trabalhos Realizados
    - Trabalhos Não Realizados
    - Provas
* * *
## Saiba mais...
Para mais informações Inicial e Técnica, acesse os links abaixo

[Inicial]:../README.md
[Técnico]:./README-Tecnico.md

- Dados [Inicial][].
- Dados [Técnico][].