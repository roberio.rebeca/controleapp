# Sistema de Controle Escolar
`Versão 1.0.0`

Objetivo deste sistema é o controle escolar dos filhos
* * *
## Divisão
* __api:__ Controle da aplicação de backend, será feita em python com django;
* __web:__ Controle da aplicação no frontend, será feita com javascript com VueJS;
* __doc:__ Documentação da aplicação.
* * *
## Requisitos
|Tecnologia|Versão|
|---|:---:|
| Python | 3.7.3 |
| Django | 2.2.0 |
| NodeJS | 11.14.0 |
| Yarn | 1.15.0 |
| VueCLI | 3.6.3 |

> OBS: Para controlar os pacotes do python, estamos utilizando o pipenv
* * *
## Python
### No ambiente de Produção, instalar os seguintes pacotes:
```bash
pipenv install django django_filters django_extensions python-decouple dj-database-url django-cors-headers django-extensions django-filter django-rest-auth djangorestframework psycopg2
```
`Caso acontecer algum erro na instalação do psycopg2, no sistema operacional Ubuntu 18.04, instalar a seguinte lib:`
```
apt install libpython2.7 libpython2.7-dev libpq-dev python-dev
```

### No ambiente de desenvolvimento, Instalar os seguintes pacotes:
```bash
pipenv install -d sqlformatter notebook
```
* * *
## VueJS
* * *
## Saiba mais...
Para mais informações Inicial e Estrutural, acesse os links abaixo

[Inicial]:../README.md
[Estrutural]:./README-Estrutural.md

- Dados [Inicial][].
- Dados [Estrutural][].